package com.hendisantika.springbootrestswagger.controller;

import com.hendisantika.springbootrestswagger.dto.AddUserRequest;
import com.hendisantika.springbootrestswagger.dto.AddUserResponse;
import com.hendisantika.springbootrestswagger.dto.FindUserResponse;
import com.hendisantika.springbootrestswagger.exception.ModelNotFoundException;
import com.hendisantika.springbootrestswagger.exception.PersistentException;
import com.hendisantika.springbootrestswagger.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-swagger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-06
 * Time: 07:11
 * To change this template use File | Settings | File Templates.
 */
@RestController
@Tag(name = "User", description = "Endpoints for handling and managing user related operations")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/users")
    @Operation(
            summary = "Create New User",
            description = "Create New User.",
            tags = {"User"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            AddUserResponse.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public AddUserResponse createUser(@Valid @RequestBody AddUserRequest addUserRequest, BindingResult bindingResult) throws PersistentException {
        if (bindingResult.hasErrors()) {
            throw new PersistentException(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }
        return userService.create(addUserRequest);
    }

    @GetMapping("/users/{id}")
    @Operation(
            summary = "finding the user by id",
            description = "finding the user by id.",
            tags = {"User"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            FindUserResponse.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public FindUserResponse findUserById(@PathVariable("id") Integer id) throws ModelNotFoundException {
        return userService.findUserById(id);
    }
}
