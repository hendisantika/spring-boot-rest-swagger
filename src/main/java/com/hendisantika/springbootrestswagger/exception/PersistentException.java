package com.hendisantika.springbootrestswagger.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-swagger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-06
 * Time: 07:03
 * To change this template use File | Settings | File Templates.
 */
public class PersistentException extends AppGenericException {

    public PersistentException(String message) {
        super(message);
    }
}
