package com.hendisantika.springbootrestswagger.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-swagger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-06
 * Time: 06:59
 * To change this template use File | Settings | File Templates.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Schema(name = "UserRequest")
public class AddUserRequest implements Serializable {

    @NotNull(message = "Name is required")
    @Schema(name = "Name of the user")
    private String name;

    @NotNull(message = "Username is required")
    @Schema(name = "Username of the user")
    private String username;

    @NotNull(message = "Password is required")
    @Schema(name = "Password of the user")
    private String password;
}
