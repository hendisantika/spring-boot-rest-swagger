package com.hendisantika.springbootrestswagger.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-swagger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-06
 * Time: 07:00
 * To change this template use File | Settings | File Templates.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
public class AddUserResponse implements Serializable {

    @JsonProperty("user_id")
    private Integer userId;

    private String username;

    @JsonProperty("created_on")
    private String createdOn;
}