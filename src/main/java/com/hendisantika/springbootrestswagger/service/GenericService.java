package com.hendisantika.springbootrestswagger.service;

import com.hendisantika.springbootrestswagger.exception.PersistentException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-swagger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-06
 * Time: 07:03
 * To change this template use File | Settings | File Templates.
 */
public interface GenericService<RQ, RS> {

    RS create(RQ rq) throws PersistentException;
}