package com.hendisantika.springbootrestswagger.service;

import com.hendisantika.springbootrestswagger.dto.AddUserRequest;
import com.hendisantika.springbootrestswagger.dto.AddUserResponse;
import com.hendisantika.springbootrestswagger.dto.FindUserResponse;
import com.hendisantika.springbootrestswagger.exception.ModelNotFoundException;
import com.hendisantika.springbootrestswagger.exception.PersistentException;
import org.springframework.stereotype.Service;

import javax.annotation.security.RolesAllowed;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-swagger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-06
 * Time: 07:06
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserService implements GenericService<AddUserRequest, AddUserResponse> {

    @Override
    @RolesAllowed("ROLE_ADMIN")
    public AddUserResponse create(AddUserRequest addUserRequest) throws PersistentException {

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        AddUserResponse addUserResponse = new AddUserResponse();
        addUserResponse.setUserId(1);
        addUserResponse.setUsername(addUserRequest.getUsername());
        addUserResponse.setCreatedOn(sdf.format(cal.getTime()));
        return addUserResponse;
    }


    @RolesAllowed("ROLE_USER")
    public FindUserResponse findUserById(Integer id) throws ModelNotFoundException {

        return FindUserResponse.builder()
                .userId(id)
                .name("Chathuranga Tennakoon")
                .username("chathuranga")
                .build();
    }
}