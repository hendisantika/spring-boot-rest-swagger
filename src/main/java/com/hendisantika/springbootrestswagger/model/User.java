package com.hendisantika.springbootrestswagger.model;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-swagger
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-06
 * Time: 06:58
 * To change this template use File | Settings | File Templates.
 */
public class User {
    private Long id;
    private String username;
    private String name;
    private String password;
    private String status;
}
