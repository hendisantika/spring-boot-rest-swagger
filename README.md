# Spring Boot REST Swagger

### Run this command to run this project :

1.  `git clone https://gitlab.com/hendisantika/spring-boot-rest-swagger`
2.  `cd spring-boot-rest-swagger`
3.  `mvn clean spring-boot:run`
4. Go to your browser http://localhost:8080/swagger-ui.html with username (naruto/password123).

### Screen shot

Swagger UI

![Swagger UI](img/SwaggerUI.png "Swagger UI")